# How to add your bot

1. Open [`participants.yaml`](participants.yaml)
2. Add your bot. Use `!!binary [your code base64 encoded]` if you need any special characters

# How to run

1. Make sure docker is running.  [Installing docker ON Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
2. Make sure you can use docker: [Post-installation](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
3. Make sure you install poetry with `pip install poetry`
4. Install all other dependencies with `poetry install`
5. Open the virtualenv with `cd Radiation-Hardened-Koth; poetry shell`
6. For testing, set `RUNS` lower in `src/main.py`.  Speed is about 2.5 "runs" per minute, YMMV.
7. Run `python3 src/main.py >log 2>&1 &`

(Tested on a brand new Ubuntu 22.04 server, but I don't think the host OS should matter too much.)

To get extra stats, run `./log2stats >stats.txt`.  This requires the log file.

# Changes inherited from the oskog97/Radiation-Hardened-Koth fork

- Tried to make every bot work, unsuccessfully.

- Changed RUNS and N_JOBS from 10 and 6 to 1000 and 1.  Running parallel
  jobs (N_JOBS>1) should be fine, but may break `log2stats` somewhat.
  Using RUNS=1000 takes several hours to complete, and generates an
  approximately 475 MB log file.  
  It took 7 hours on a single core on a 3.5 GHz Xeon E3-1225 v3 (~Core i7 4xxx)

- Added `log2stats` which gathers some additional statistics from the log file.

