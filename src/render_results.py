import collections
from html import escape
import statistics
import typing
import data


def render_code(
    code: typing.Union[str, bytes], occurences: collections.Counter[int]
) -> str:
    if isinstance(code, str):
        code = code.encode("utf-8")

    max_value = max(occurences.values()) if occurences.values() else 1

    def get_color(b):
        p = b / max(1, max_value)
        if p > 0.5:
            return f"#{int(p*30-15):<1x}0{int(30-p*30):<1x}"
        else:
            return f'#0{int(7.5-p*15):<1x}{int(p*30):<1x}'

    out = "<code style='background-color:lightgrey; padding: 5px; margin: 1rem; display: block; max-width: 50vw; overflow-x: auto'>"
    for i, char in enumerate(code):
        out += f"<span style='color: {get_color(occurences[i])}; white-space: pre; {'text-decoration: underline;'if char==0x20 else ''}'>{escape(repr(bytes([char]))[2:-1])}</span>"
    return out + "</code>"


def render_output(results: list[data.Participant]):
    results.sort(
        key=lambda i: (statistics.median(i.score), statistics.mean(i.score)),
        reverse=True,
    )

    with open("results.html", "w") as f:
        f.write(
            """
            <!DOCTYPE html>
            <html>
            <head>
            <title>Results</title>
            </head>
            <body>
            <p>
                Green = Less frequent, blue = medium frequent, red = more frequent
            </p>
            <table>
                <thead>
                    <tr>
                        <th>Player</th>
                        <th>Code (Vulnerable/Fatal)</th>
                        <th>Length</th>
                        <th>Total Rounds Survived</th>
                        <th>Median Rounds Survived</th>
                        <th>Total Score</th>
                        <th>Median Score</th>
                    </tr>
                </thead>
                <tbody>

        """
        )
        for participant in results:
            f.write(
                f"""
                <tr>
                    <td><a href="{escape(str(participant.link))}">{escape(participant.name)}</a></td>
                    <td>
                        {render_code(participant.code, participant.vulnerable_characters)}
                        {render_code(participant.code, participant.fatal_characters)}<br>
                    <td>{len(participant.code)}</td>
                    <td>{sum(participant.rounds_survived)}</td>
                    <td>{statistics.median(participant.rounds_survived)}</td>
                    <td>{sum(participant.score)}</td>
                    <td>{statistics.median(participant.score)}</td>
                </tr>
            """
            )

        f.write("</tbody></table></body></html>")
