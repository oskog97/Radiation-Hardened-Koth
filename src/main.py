import asyncio
from asyncio import subprocess
import random
import re
import typing
import yaml
import os
import dock_controller
import base64
import data
import render_results
import time

CONTAINER_NAME = "radiation-hardened-koth"

error_log = None


RUNS = 100
TIMEOUT = 1.2
N_JOBS = 1


async def set_up_container(container_id=0) -> dock_controller.ContainerManager:
    global error_log

    container_name = CONTAINER_NAME + str(container_id)
    ps_process = await subprocess.create_subprocess_exec(
        "docker", "ps", "-af", f"name={container_name}", stdout=subprocess.PIPE
    )
    result, _ = await ps_process.communicate()
    if container_name.encode("utf-8") not in result:
        return await dock_controller.ContainerManager.new(name=container_name)

    start_process = await subprocess.create_subprocess_exec(
        "docker",
        "start",
        "-ia",
        f"{container_name}",
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        # stderr=(error_log:=open("error.log", "wb")),
    )

    return dock_controller.ContainerManager(start_process)


def parse_game_end_message(
    message: bytes, competitor: bytes
) -> typing.Optional[tuple[int, bytes]]:
    match = re.match(rb"(\d+) (.)\n\n*", message, re.DOTALL)
    if match:
        position = int(typing.cast(bytes, match.group(1)).decode("utf-8"))
        character = typing.cast(bytes, match.group(2))
        if position < len(competitor) and competitor[position] == character[0]:
            return position, character
    return None


async def run_all(container_id: int):
    with open(
        os.path.abspath(os.path.join(__file__, "..", "..", "participants.yaml"))
    ) as f:
        config: data.Config = data.Config(**yaml.load(f, yaml.Loader))
    # print(config)

    async with await set_up_container(container_id) as s:
        await s.run("apt-get -y update")
        await s.run("apt-get install -y coreutils ")

        for participant in config.participants:
            await s.run(participant.setup)
            # print(type(participant.code))
            # print(participant.name)

        # return

        for run_counter in range(RUNS):
            for participant in config.participants:
                participant.score.append(0)
                participant.rounds_survived.append(0)

            participants = [
                data.ParticipantInstance(
                    participant=i,
                    modified_code=i.code
                    if isinstance(i.code, bytes)
                    else i.code.encode("utf-8"),
                )
                for i in config.participants
            ]

            for participant_instance in participants[:]:
                if len(participant_instance.modified_code) > 256:
                    print(
                        f"Participant {participant_instance.participant.name} was elminated for ebing too long: {len(participant_instance.modified_code)} (original: {len(participant_instance.participant.code)}, {type(participant_instance.participant.code)})"
                    )
                    participants.remove(participant_instance)
            random.shuffle(participants)

            i = 0
            while len(participants) > 1:
                await s.run(
                    f"echo -n {base64.b64encode(participants[i%len(participants)].modified_code).decode('utf-8')} | base64 --decode > code"
                )

                perpetrator = participants[i % len(participants)]
                victim = participants[(i + 1) % len(participants)]
                lines = [
                    i.strip()
                    for i in perpetrator.participant.run.split("\n")
                    if i.strip()
                ]

                start_time = time.perf_counter()

                for line in lines[:-1]:
                    await s.run(line, print_output=False)
                result = b"\n".join(
                    [
                        i
                        async for i in s.get_output(
                            f"echo -n {base64.b64encode(victim.modified_code).decode('utf-8')} | base64 --decode | timeout {TIMEOUT+0.1} {lines[-1]} | head -c1000"
                        )
                    ]
                )

                end_time = time.perf_counter()

                score = parse_game_end_message(result, victim.modified_code)

                if score is not None and end_time > start_time + TIMEOUT:
                    score = None
                    print(perpetrator.participant.name, "timed out")

                if score is not None:
                    victim.modified_code = (
                        victim.modified_code[: score[0]]
                        + victim.modified_code[score[0] + 1 :]
                    )
                    victim.previous_assailant = perpetrator
                    perpetrator.participant.rounds_survived[-1] += 1
                    print(
                        f"removed {score[1]!r} from {victim.participant.name!r}, new value: {victim.modified_code!r}"
                    )
                    victim.participant.vulnerable_characters[
                        victim.get_original_index(score[0])
                    ] += 1
                    victim.last_removed_index = victim.get_original_index(score[0])

                    assert (
                        q := (
                            (
                                victim.participant.code
                                if isinstance(victim.participant.code, bytes)
                                else victim.participant.code.encode("utf-8")
                            )[victim.get_original_index(score[0])]
                        )
                    ) == score[1][
                        0
                    ], f"Value at index {score[0]} (modified to {victim.get_original_index(score[0])} was {score[1]!r} instead of {q!r}"
                    victim.removed_letters.add(victim.get_original_index(score[0]))

                    i += 1
                else:
                    print(
                        f"Player {perpetrator.participant.name} was eliminated, output: "
                    )
                    print(repr(result))
                    participants.remove(perpetrator)
                    if perpetrator.previous_assailant is not None:
                        perpetrator.previous_assailant.participant.score[-1] += 1
                        print(
                            f"new score of {perpetrator.previous_assailant.participant.name} {perpetrator.previous_assailant.participant.score}"
                        )

                    perpetrator.participant.fatal_characters[
                        perpetrator.last_removed_index
                    ] += 1

        return config.participants
        # print(config.participants)


def combine_participant(*participants: data.Participant):
    p = participants[0]
    for i in participants[1:]:
        assert p.name == i.name
        p.score += i.score
        p.rounds_survived += i.rounds_survived
        for key, value in i.vulnerable_characters.items():
            p.vulnerable_characters[key] += value
        for key, value in i.fatal_characters.items():
            p.fatal_characters[key] += value
    return p


async def combine_all():
    results = await asyncio.gather(*(run_all(i) for i in range(0, N_JOBS)))

    combined_participants = [combine_participant(*i) for i in zip(*results)]

    render_results.render_output(combined_participants)


if __name__ == "__main__":
    asyncio.run(combine_all())
