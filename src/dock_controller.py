from asyncio import subprocess
import typing
import uuid
import contextlib
import asyncio


class ContainerManager:
    process: subprocess.Process

    def __init__(self, process: subprocess.Process):
        self.process = process

    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        if await self.is_running():
            await self.run("exit")
        if await self.is_running():
            self.process.kill()

    @classmethod
    async def new(
        cls, container: str = "ubuntu", command: str = "bash", name="awesome-bardeen"
    ) -> "ContainerManager":
        process = await subprocess.create_subprocess_exec(
            "docker",
            "run",
            "-i",
            "--name",
            name,
            container,
            command,
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )
        return ContainerManager(process)

    async def is_running(self):
        with contextlib.suppress(asyncio.TimeoutError):
            await asyncio.wait_for(self.process.wait(), 1e-6)
        return self.process.returncode is None

    async def get_output(self, command: str):
        print("$ \33[0;33m", command, "\33[0m")
        command_id = uuid.uuid4()
        self.stdin.write((command + "\n").encode("utf-8"))
        self.stdin.write(f"echo\necho {command_id}\n".encode("utf-8"))

        while (out := (await self.stdout.readline())) != str(command_id).encode(
            "utf-8"
        ) + b"\n" and await self.is_running():
            if out:
                yield out

    async def run(self, command: str, print_output=True):
        async for line in self.get_output(command):
            if print_output:
                print(">\t\33[0;35m", line.decode("utf-8").strip("\n"), "\33[0m")

    @property
    def stdin(self) -> asyncio.StreamWriter:
        return typing.cast(asyncio.StreamWriter, self.process.stdin)

    @property
    def stdout(self) -> asyncio.StreamReader:
        return typing.cast(asyncio.StreamReader, self.process.stdout)
