import typing
import pydantic
import collections
import dataclasses


class Participant(pydantic.BaseModel):
    name: str
    container: int = 0
    setup: str
    run: str
    code: str | bytes
    score: list[int] = pydantic.Field(default_factory=list)
    rounds_survived: list[int] = pydantic.Field(default_factory=list)
    vulnerable_characters: collections.Counter[int] = pydantic.Field(
        default_factory=collections.Counter
    )
    fatal_characters: collections.Counter[int] = pydantic.Field(
        default_factory=collections.Counter
    )
    link: typing.Optional[str] = None


class Config(pydantic.BaseModel):
    participants: list[Participant]


@dataclasses.dataclass()
class ParticipantInstance:
    participant: Participant
    modified_code: bytes
    removed_letters: set = dataclasses.field(default_factory=set)
    previous_assailant: typing.Optional["ParticipantInstance"] = None
    last_removed_index: int = 300

    def __eq__(self, other):
        return self is other

    def get_original_index(self, new_index: int) -> int:
        for i in sorted(self.removed_letters):
            if i <= new_index:
                new_index += 1
        return new_index
